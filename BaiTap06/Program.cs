﻿using System;

namespace BaiTap06
{
    class Program
    {
        static void Main(string[] args)
        {
            // Viết hàm kiểm tra tam giác, khi  nhập 3 cạnh của 1 tam giác.
            Console.OutputEncoding = System.Text.Encoding.UTF8;
            while (true)
            {
                Console.Write("Nhập vào cạnh thứ nhất: ");
                double a = double.Parse(Console.ReadLine());
                Console.Write("Nhập vào cạnh thứ hai: ");
                double b = double.Parse(Console.ReadLine());
                Console.Write("Nhập vào cạnh thứ ba: ");
                double c = double.Parse(Console.ReadLine());
                //// Điều kiện tam giác
                if (a + b > c && b + c > a && c + a > b)
                {
                    //// Định nghĩa độ lệch cho phép
                    double epsilon = 0.0001;
                    if (a == b && b == c)
                        Console.WriteLine("Tam giác đều");
                    else if (a == b || b == c || c == a)
                    {
                        //// Kiểm tra thêm xem có phải là tam giác vuông nữa không
                        if (Math.Abs((a * a) + b * b - c * c) <= epsilon
                            || Math.Abs(b * b + c * c - a * a) <= epsilon
                            || Math.Abs(c * c + a * a - b * b) <= epsilon)
                            Console.WriteLine("Tam giác vuông cân");
                        else
                            Console.WriteLine("Tam giác cân");
                    }
                    else if (Math.Abs(a * a + b * b - c * c) <= epsilon
                            || Math.Abs(b * b + c * c - a * a) <= epsilon
                            || Math.Abs(c * c + a * a - b * b) <= epsilon)
                        Console.WriteLine("Tam giác vuông");
                    else if (a * a + b * b + epsilon < c * c
                        || b * b + c * c + epsilon < a * a
                        || c * c + a * a + epsilon < b * b)
                        Console.WriteLine("Tam giác tù");
                    else
                        Console.WriteLine("Tam giác nhọn");
                }
                else
                    Console.WriteLine("Không phải tam giác");
                Console.WriteLine("======================");
            }
        }
    }
}
