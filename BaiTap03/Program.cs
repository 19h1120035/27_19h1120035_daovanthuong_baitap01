﻿using System;

namespace BaiTap03
{
    class Program
    {
        //Viết hàm giải phương trình bậc nhất ax + b = 0.
        static void Main(string[] args)
        {
            Console.OutputEncoding = System.Text.Encoding.UTF8;
            Console.WriteLine("Nhập vào số thứ nhất: ");
            double a = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Nhập vào số thứ hai: ");
            double b = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine($"Phương trình bậc nhất: {a}X + {b} = 0");
            if (a == 0)
            {
                if (b == 0)
                    Console.WriteLine("Phương trình vô số nghiệm !");
                else
                    Console.WriteLine("Phương trình vô nghiệm !");
            }
            else
                Console.WriteLine($"Phương trình có nghiệm là: x = {(-b) / a}");
        }
    }
}
