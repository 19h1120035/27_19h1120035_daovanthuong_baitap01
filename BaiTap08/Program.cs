﻿using System;

namespace BaiTap08
{
    class Program
    {
        //Viết hàm tìm giá trị đầu tiên có chữ số đầu tiên là chữ số lẻ trong mảng 1 chiều các số nguyên. 
        static void Main(string[] args)
        {
            Console.OutputEncoding = System.Text.Encoding.UTF8;
            Console.Write("Nhập vào số phần tử của mảng: ");
            int n = int.Parse(Console.ReadLine());
            int[] arr = new int[n];
            Random random = new Random();
            for (int i = 0; i < n; i++)
                arr[i] = random.Next(100);
            Console.Write("Các phần tử có trong mảng: ");
            foreach (int i in arr)
                Console.Write(i + " ");
            Console.WriteLine();
            Console.WriteLine($"Phần tử đầu tiên của mảng là: {arr[0]}");
            string firstElement = $"{arr[0]}";
            if (firstElement[0] % 2 != 0)
                Console.WriteLine($"Giá trị đầu tiên có chữ số đầu tiên là chữ số lẻ trong mảng là: {firstElement[0]}");
            else
                Console.WriteLine("Không có giá trị đầu tiên có chữ số đầu tiên là số lẻ !");
        }
    }
}
