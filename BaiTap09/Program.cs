﻿using System;

namespace BaiTap09
{
    class Program
    {
        //Viết hàm tính tổng các phần tử trong mảng
        static void Main(string[] args)
        {
            Console.OutputEncoding = System.Text.Encoding.UTF8;
            Console.Write("Nhập vào số phần tử của mảng: ");
            int n = int.Parse(Console.ReadLine());
            int[] arr = new int[n];
            Random random = new Random();
            for (int i = 0; i < n; i++)
                arr[i] = random.Next(100);
            Console.Write("Các phần tử trong mảng: ");
            foreach (int i in arr)
                Console.Write(i+"  ");
            int sum = 0;
            foreach (int i in arr)
                sum += i;
            Console.WriteLine();
            Console.WriteLine("Tổng các phần tử trong mảng: "+sum);
        }
    }
}
