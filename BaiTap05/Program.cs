﻿using System;

namespace BaiTap05
{
    class Program
    {
        //Viết hàm kiểm tra 1 số có phải là số nguyên tố hay không.
        static void Main(string[] args)
        {
            Console.OutputEncoding = System.Text.Encoding.UTF8;
            Console.WriteLine("Nhập vào số nguyên n: ");
            int n = int.Parse(Console.ReadLine());
            int count = 0;
            for (int i = 1; i <= n; i++)
                if (n % i == 0)
                    count++;
            if (count == 2)
                Console.WriteLine($"{n} là một số nguyên tố !");
            else
                Console.WriteLine($"{n} không phải số nguyên tố !.");
        }
    }
}
