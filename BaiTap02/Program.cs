﻿using System;

namespace BaiTap02
{
    class Program
    {
        //Viết hàm kiểm tra 2 số thực cùng dấu hay khác dấu.
        static void Main(string[] args)
        {
            Console.OutputEncoding = System.Text.Encoding.UTF8;
            Console.WriteLine("Nhập vào số thực thứ nhất: ");
            double a = double.Parse(Console.ReadLine());
            Console.WriteLine("Nhập vào số thực thứ hai: ");
            double b = double.Parse(Console.ReadLine());
            if (a >= 0 && b >= 0)
                Console.WriteLine($"Hai số thực {a} và {b} cùng dấu !");
            else
                Console.WriteLine($"Hai số thực {a} và {b} khác dấu !");
        }
    }
}
