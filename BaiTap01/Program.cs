﻿using System;
using System.Text;

namespace BaiTap01
{
    class Program
    {
        //Viết hàm tìm số lớn nhất trong 3 số thực được nhập từ bàn phím.
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;
            Console.WriteLine("Nhập vào số thực thứ nhất : ");
            double a = double.Parse(Console.ReadLine());
            Console.WriteLine("Nhập vào số thực thứ hai: ");
            double b = double.Parse(Console.ReadLine());
            Console.WriteLine("Nhập vào số thực thứ ba: ");
            double c = double.Parse(Console.ReadLine());
            double maxAB = Math.Max(a, b);
            double maxABC = Math.Max(maxAB, c);
            Console.WriteLine($"Số lớn nhất trong 3 số thực {a} , {b} , {c} là: {maxABC}");
        }

    }
}
