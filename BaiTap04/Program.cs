﻿using System;

namespace BaiTap04
{
    class Program
    {
        //Viết hàm tính tổng của dãy số sau: S(n) = 1 + 2 + 3 + … + n (trong đó n là số nguyên dương).
        static void Main(string[] args)
        {
            Console.OutputEncoding = System.Text.Encoding.UTF8;
            Console.WriteLine("Nhập vào số nguyên n: ");
            int n = int.Parse(Console.ReadLine());
            int sum = 0;
            for (int i = 1; i <= n; i++)
                sum += i;
            Console.WriteLine($"Tổng của dãy số 1 + 2 + ... + {n} = {sum}");
        }
    }
}
