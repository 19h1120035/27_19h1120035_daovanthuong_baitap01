﻿using System;

namespace BaiTap07
{
    class Program
    {
        //Viết hàm nhập, xuất mảng 1 chiều các số nguyên.
        static void Main(string[] args)
        {
            Console.OutputEncoding = System.Text.Encoding.UTF8;
            Console.Write("Nhập số phần tử của mảng: ");
            int n = int.Parse(Console.ReadLine());
            int[] arr = new int[n];
            Random random = new Random();
            for (int i = 0; i < n; i++)
                arr[i] = random.Next(100);
            Console.Write("Các phần tử trong mảng: ");
            foreach (int i in arr)
                Console.Write(i + "  ");
            Console.WriteLine();
        }
    }
}
